package fr.da2i.projet_web_back.controleur;

import fr.da2i.projet_web_back.modele.Depenses;
import fr.da2i.projet_web_back.modele.DepensesRepository;
import fr.da2i.projet_web_back.modele.Personne;
import fr.da2i.projet_web_back.modele.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class DepensesController {

    @Autowired
    private DepensesRepository depensesRepository;

    @GetMapping("")
    public Iterable<Depenses> selectAll(){
        return depensesRepository.findAll();
    }

    @PostMapping("/add")
    public Depenses addDepenses(@RequestBody Depenses depenses) {
        return depensesRepository.save(depenses);
    }
}
