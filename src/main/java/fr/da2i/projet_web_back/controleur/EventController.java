package fr.da2i.projet_web_back.controleur;

import fr.da2i.projet_web_back.modele.Event;
import fr.da2i.projet_web_back.modele.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/event")
public class EventController {
    @Autowired
    private EventRepository eventRepository;

    @GetMapping("")
    public Iterable<Event> selectAll(){
        return eventRepository.findAll();
    }

    @PostMapping("/add")
    public Event addEvent(@RequestBody Event event){
        return eventRepository.save(event);
    }
}
