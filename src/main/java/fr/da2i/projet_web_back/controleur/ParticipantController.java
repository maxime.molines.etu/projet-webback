package fr.da2i.projet_web_back.controleur;

import fr.da2i.projet_web_back.modele.Depenses;
import fr.da2i.projet_web_back.modele.DepensesRepository;
import fr.da2i.projet_web_back.modele.Participant;
import fr.da2i.projet_web_back.modele.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class ParticipantController {

    @Autowired
    private ParticipantRepository participantRepository;

    @GetMapping("")
    public Iterable<Participant> selectAll(){
        return participantRepository.findAll();
    }

    @PostMapping("/add")
    public Participant addParticipant(@RequestBody Participant participant) {
        return participantRepository.save(participant);
    }
}
