package fr.da2i.projet_web_back.controleur;

import fr.da2i.projet_web_back.modele.Personne;
import fr.da2i.projet_web_back.modele.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class PersonneController {

    @Autowired
    private PersonneRepository personRepository;

    @GetMapping("")
    public Iterable<Personne> selectAll(){
        return personRepository.findAll();
    }

    @PostMapping("/add")
    public Personne addPerson(@RequestBody Personne person) {
        return personRepository.save(person);
    }
}
