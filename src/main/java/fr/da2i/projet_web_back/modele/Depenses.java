package fr.da2i.projet_web_back.modele;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * $table.getTableComment()
 */
@Entity
@Table(name = "depenses")
@Data
public class Depenses implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * $column.getColumnComment()
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ref", nullable = false)
    private Long ref;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "libelle", nullable = false)
    private String libelle;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "dddep", nullable = false)
    private Date dddep;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "montant", nullable = false)
    private Long montant;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "id", nullable = false)
    private Long id;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "pseudo", nullable = false)
    private String pseudo;

}
