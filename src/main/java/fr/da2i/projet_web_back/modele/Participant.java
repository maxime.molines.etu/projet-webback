package fr.da2i.projet_web_back.modele;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * $table.getTableComment()
 */
@Entity
@Table(name = "participant")
@Data
@IdClass(ParticipantId.class)
public class Participant implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * $column.getColumnComment()
     */
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    /**
     * $column.getColumnComment()
     */
    @Id
    @Column(name = "pseudo", nullable = false)
    private String pseudo;

}
