package fr.da2i.projet_web_back.modele;

import java.io.Serializable;

public class ParticipantId implements Serializable {
    private Long id;
    private String pseudo;

    public ParticipantId(Long id, String pseudo){
        this.id=id;
        this.pseudo=pseudo;
    }
}
