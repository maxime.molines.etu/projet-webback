package fr.da2i.projet_web_back.modele;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DepensesRepository extends JpaRepository<Depenses, Long>, JpaSpecificationExecutor<Depenses> {

}