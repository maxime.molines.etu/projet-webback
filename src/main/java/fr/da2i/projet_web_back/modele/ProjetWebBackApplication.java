package fr.da2i.projet_web_back.modele;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetWebBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetWebBackApplication.class, args);
	}

}
