package fr.da2i.projet_web_back.modele;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Date;

/**
 * $table.getTableComment()
 */
@Entity
@Table(name = "personne")
@Data
public class Personne implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * $column.getColumnComment()
     */
    @Id
    @Column(name = "pseudo", nullable = false)
    private String pseudo;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "nom", nullable = false)
    private String nom;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "prenom", nullable = false)
    private String prenom;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "ddnaiss")
    private Date ddnaiss;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "mail", nullable = false)
    private String mail;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "mdp", nullable = false)
    private String mdp;

}
