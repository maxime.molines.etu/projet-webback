package fr.da2i.projet_web_back.modele;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * $table.getTableComment()
 */
@Entity
@Table(name = "event")
@Data
public class Event implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * $column.getColumnComment()
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "titre", nullable = false)
    private String titre;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "description")
    private String description;

    /**
     * $column.getColumnComment()
     */
    @Column(name = "etat")
    private String etat;

}
